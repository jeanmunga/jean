import java.util.Scanner;

public class HandlingBadInput {
    public static void main(String[] args) {
        String regex = "[9]";
        String regex1 = "[0]";
        System.out.println("what is the rate of return? ");
        Scanner scanner = new Scanner(System.in);

        String r;
        for(r = scanner.next(); !r.matches(regex); r = scanner.next()) {
            if (r.matches(regex1)) {
                System.out.println("Input number bigger than 0");
            }

            System.out.println("Sorry. That's not a valid input.");
            System.out.println("what is the rate of return? ");
        }

        int years = 72 / Integer.parseInt(r);
        System.out.println("It will take " + years + " years to double your initial investment.");
    }
}
