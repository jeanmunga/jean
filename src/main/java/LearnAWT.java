import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LearnAWT extends Frame {
    TextField tf1;
    TextField tf2;
    Label l1;
    Button b;
    Button c;
    Button d;
    Button e;
    int a;
    int bc;

    LearnAWT() {
        setTitle("Adder");
        tf1 = new TextField();
        tf1.setBounds(100, 50, 85, 20);
        tf2 = new TextField();
        tf2.setBounds(100, 100, 85, 20);

        b = new Button("Add");
        b.setBounds(110,140,50,50);

        c = new Button("Minus");
        c.setBounds(110,190,50,40);

        d = new Button("Multiply");
        d.setBounds(110,225,70,40);

        e = new Button("Divide");
        e.setBounds(110,260,50,40);

        l1 = new Label("");
        l1.setBounds(100, 120, 85, 20);

        add(b);
        add(c);
        add(d);
        add(e);
        add(tf1);
        add(tf2);
        add(l1);

        setSize(300,300);
        setVisible(true);



        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());



                if(a < 0 || bc < 0){
                    l1.setText("Input positive number");
                }else{

                    int c = add(a,bc);

                    l1.setText("Their sum is = " + String.valueOf(c));

                }

            }
        });

        c.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());

                if(a < 0 || bc < 0){
                    l1.setText("Input positive number");
                }else{


                    int c1 = minus(a,bc);

                    l1.setText("Their sum is = " + String.valueOf(c1));
                }

            }
        });

        d.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());

                if(a < 0 || bc < 0){
                    l1.setText("Input positive number");
                }else{


                    int c1 = multiple(a,bc);

                    l1.setText("Their sum is = " + String.valueOf(c1));
                }

            }
        });

        e.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());

                if(a < 0 || bc < 0){
                    l1.setText("Input positive number");
                }else{


                    int c1 = divide(a,bc);

                    l1.setText("Their sum is = " + String.valueOf(c1));
                }

            }
        });
    }
    public int add(int a,int b){
        return a+b;
    }
    public int minus(int a,int b){
        return a-b;
    }

    public int multiple(int a,int b){
        return a*b;
    }
    public int divide(int a,int b){
        return a/b;
    }
    public static void main(String []args) {
        new LearnAWT();
    }
}