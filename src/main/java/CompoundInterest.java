import java.util.Scanner;

public class CompoundInterest {
     public static void main(String[] args) {
         double StartAmount;
         double Years;
         double InterestRate;
         double YearPeriod;

         Scanner scanner = new Scanner(System.in);

         System.out.println("What is the principal amount?");
         StartAmount = scanner.nextDouble();

         System.out.println("What is the rate? ");
         Years = scanner.nextDouble();

         System.out.println("What is the number of years? ");
         InterestRate = scanner.nextDouble();

         System.out.println("What is the number of times the interest is compounded per year?");
         YearPeriod = scanner.nextDouble();

        double CompoundInterest =  StartAmount * Math.pow(1 + (InterestRate/100 / YearPeriod), YearPeriod * Years);
         System.out.println("");
    }
}
