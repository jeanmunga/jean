import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class AlcoholCalculator {
    public static void main(String[] args) {
//        double Weight;
//        String Gender;
//        int NODrinks;
//        double VolumeDrinks;
//        double TimeSince;
//        double r;
//        double BAC;
//
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("What's your gender");
//        Gender = scanner.nextLine();
//
//        System.out.println("How much do you weight ");
//        Weight = scanner.nextDouble();
//
//
//        System.out.println("How many drinks have you had");
//        NODrinks = scanner.nextInt();
//
//        System.out.println("How many litres is all together");
//        VolumeDrinks = scanner.nextDouble();
//
//        System.out.println("When was the last time you had a drink");
//        TimeSince = scanner.nextInt();
//
//        double ounces = VolumeDrinks*VolumeDrinks*5.14;
//
//        if(Objects.equals(Gender, "male")){
//            r = 0.73;
//            BAC = (ounces *5.14/Weight*r)-(0.015*TimeSince);
//            System.out.println(BAC);
//
//        }else if(Objects.equals(Gender, "female")){
//            r = 0.66;
//            BAC = (ounces *5.14/Weight*r)-0.015*TimeSince;
//            System.out.println(BAC);
//
//        }


        Scanner input = new Scanner(System.in);

        // Prompt user for weight in pounds
        System.out.print("Enter your weight in pounds: ");
        double weight = input.nextDouble();

        // Prompt user for gender
        System.out.print("Enter your gender (M/F): ");
        String gender = input.next();

        // Prompt user for number of drinks
        System.out.print("Enter the number of drinks you've had: ");
        int numDrinks = input.nextInt();

        // Prompt user for alcohol by volume (ABV) of drinks consumed
        System.out.print("Enter the alcohol by volume (ABV) of the drinks you've had (as a decimal): ");
        double abv = input.nextDouble();

        // Prompt user for time since last drink
        System.out.print("Enter the time since your last drink in hours: ");
        double timeSinceLastDrink = input.nextDouble();

        System.out.print("Enter the state your are in: ");
        String State = input.next();

        Map<String, Double> map = new HashMap<String,Double>();
        map.put("United states of America", 0.08);
        map.put("England", 0.18);
        map.put("France", 0.15);


        // Define the alcohol distribution ratio (r) based on gender
        double r;
        if (gender.equalsIgnoreCase("m")) {
            r = 0.73;
        } else if (gender.equalsIgnoreCase("f")) {
            r = 0.66;
        } else {
            System.out.println("Invalid gender input.");
            return;
        }

        // Calculate total alcohol consumed (A) in ounces
        double A = numDrinks * abv * 5.14;

        // Calculate BAC using the formula
        double BAC = (A / (weight * r)) - (0.015 * timeSinceLastDrink);

        // Display the calculated BAC
        System.out.printf("Your blood alcohol content (BAC) is %.3f%%", BAC * 100);

//        for (Map.Entry<String, Double> entry : map.entrySet()) {
//            if (BAC> entry.getValue()){
//                System.out.println("It is not legal for you to drive.");
//            }
//            else{
//                System.out.println("It is legal for you to drive.");
//            }
//
//        }

        Double StateLimit = map.get(State);
        if(map.containsKey(State)){
            if(BAC>StateLimit){
                System.out.println("It is not legal for you to drive.");
            }
            else{
                System.out.println("It is legal for you to drive.");
            }
        }






    }
}
