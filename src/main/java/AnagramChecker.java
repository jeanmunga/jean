import java.util.Arrays;
import java.util.Scanner;

public class AnagramChecker {
    public static void main(String[] args) {

        String word1;
        String word2;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter two strings and I'll tell you if they are anagram");

        System.out.println("Enter the first word");
        word1 = scanner.nextLine();

        System.out.println("Enter the second word");
        word2 = scanner.nextLine();

        AnagramChecker anagramChecker = new AnagramChecker();
//        anagramChecker.isAnagram(word1,word2);
//
//        if(word1.length() != word2.length()) {
//            isEqual = false;
//        } else {
//            for(int i=0; i<str1.length(); i++) {
//                if(str1.charAt(i) != str2.charAt(i)) {
//                    isEqual = false;
//                    break;
//                }
//            }
//        }

//        if(anagramChecker.isAnagram(word1, word2)) {
//            System.out.println("The two strings are not equal.");
//        } else {
//            System.out.println("The two strings are equal.");
//        }

//        if(anagramChecker.isAnagram(word1, word2)) {
//            System.out.println("The two strings are equal.");
//        } else {
//            System.out.println("The two strings are not equal.");
//        }

//        if(compareStrings(word1, word2)) {
//            System.out.println("The two strings are equal.");
//        } else {
//            System.out.println("The two strings are not equal.");
//        }

        if (compareStrings(word1,word2)) {
            System.out.println(word1 + " and " + word2 + " are anagrams.");
        } else {
            System.out.println(word1 + " and " + word2 + " are not anagrams.");
        }


    }
    public boolean isAnagram(String word1,String word2){

        if(word1.length() != word2.length()) {
            return false;
        } else {
            for(int i=0; i<word1.length(); i++) {
                if(word1.charAt(i) != word2.charAt(i)) {
                    return false;
//                    break;
                }
            }
            return true;
        }


//        if(word1.length()==word2.length()){
//            System.out.println(word1 +" and "+ word2 + " are anagram" );
//            return true;
//        }else{
//            System.out.println("not anagram");
//            return false;
//        }
    }
    public static boolean compareStrings(String word1, String word2) {
        // Convert the words to arrays of characters
        char[] letters1 = word1.toCharArray();
        char[] letters2 = word2.toCharArray();

        // Sort the arrays of characters using the Arrays.sort() method
        Arrays.sort(letters1);
        Arrays.sort(letters2);

        // Check if the sorted arrays are equal using a for loop
//        boolean areAnagrams = true;
        if (letters1.length != letters2.length) {
            return false;
        } else {
            for (int i = 0; i < letters1.length; i++) {
                if (letters1[i] != letters2[i]) {
                    return false;
//                    break;
                }
            }
            return true;
        }

    }
}
