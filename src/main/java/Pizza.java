import java.util.Scanner;

public class Pizza {
    public static void main(String[] args) {
        double people;
        double pizza;

        Scanner scanner = new Scanner(System.in);

        System.out.println("How many people?");

        people = scanner.nextInt();

        System.out.println("How many pizzas do you have?");

        pizza = scanner.nextInt();

        double sum = pizza/people*people;

        System.out.println("Each person gets " + sum + " piece" + (sum==1?"":"s" + " of pizza"));





    }

}
