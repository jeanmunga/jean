import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatingInputs {
    public static void main(String[] args) {
        String FirstName;
        String LastName;
        int Zipcode;
        String EmployeeID;
        String Pattern;

        ValidatingInputs validatingInputs = new ValidatingInputs();

        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your first name");
        FirstName = scanner.next();
        validatingInputs.FirstName(FirstName);


        System.out.println("What is your last name");
        LastName = scanner.next();
        validatingInputs.LastName(LastName);


        System.out.println("What is your zipcode");
        Zipcode = scanner.nextInt();
        validatingInputs.Zipcode(Zipcode);


        System.out.println("What is your EmployeeID");
        EmployeeID = scanner.next();
        validatingInputs.EmployeeID(EmployeeID);



    }
    public void FirstName(String FirstName){
        if(FirstName.length()<2){
            System.out.println(FirstName +" is not a valid first name. It is too short.");
        }else {
            System.out.println(FirstName);
        }

    }
    public void LastName(String LastName){
        if(LastName.length()<1){
            System.out.println("The last name must be filled in");
        }else {
            System.out.println(LastName);
        }


    }
    public void Zipcode(int Zipcode){
        System.out.println(Zipcode);
    }
    public void EmployeeID(String EmployeeID){
        String Pattern = "[a-zA-Z]{2}-\\d{4}";

        if (EmployeeID.matches(Pattern)) {
            System.out.println("Employee ID is valid.");
        } else {
            System.out.println("Employee ID is invalid.");
        }
    }
}
