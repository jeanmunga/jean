import java.util.*;

public class NumbersToNames {
    public static void main(String[] args) {
        int number;

        Scanner scanner = new Scanner(System.in);

//        switch (number){
//            case 1:
//                System.out.println("The name of the month is January");
//                break;
//            case 2:
//                System.out.println("The name of the month is February");
//                break;
//            case 3:
//                System.out.println("The name of the month is March");
//                break;
//            case 4:
//                System.out.println("The name of the month is April");
//                break;
//            case 5:
//                System.out.println("The name of the month is May");
//                break;
//            case 6:
//                System.out.println("The name of the month is June");
//                break;
//            case 7:
//                System.out.println("The name of the month is July");
//                break;
//            case 8:
//                System.out.println("The name of the month is August");
//                break;
//            case 9:
//                System.out.println("The name of the month is September");
//                break;
//            case 10:
//                System.out.println("The name of the month is October");
//                break;
//            case 11:
//                System.out.println("The name of the month is November");
//                break;
//            case 12:
//                System.out.println("The name of the month is December");
//                break;
//            default:
//                System.out.println("Invalid Month");
//
//        }

//        Map<Integer, Runnable> actions = new HashMap<>();
//        actions.put(1, () -> {
//            System.out.println("The name of the month is January");
//            // do something
//        });
//        actions.put(2, () -> {
//            System.out.println("The name of the month is February");
//            // do something else
//        });
//        actions.put(3, () -> {
//            System.out.println("The name of the month is March");
//            // do something different
//        });
//        actions.put(4, () -> {
//            System.out.println("The name of the month is April");
//            // do something different
//        });
//        actions.put(5, () -> {
//            System.out.println("The name of the month is May");
//            // do something different
//        });
//        actions.put(6, () -> {
//            System.out.println("The name of the month is June");
//            // do something different
//        });
//        actions.put(7, () -> {
//            System.out.println("The name of the month is July");
//            // do something different
//        });
//        actions.put(8, () -> {
//            System.out.println("The name of the month is August");
//            // do something different
//        });
//        actions.put(9, () -> {
//            System.out.println("The name of the month is September");
//            // do something different
//        });
//        actions.put(10, () -> {
//            System.out.println("The name of the month is October");
//            // do something different
//        });
//        actions.put(11, () -> {
//            System.out.println("The name of the month is November");
//            // do something different
//        });
//        actions.put(12, () -> {
//            System.out.println("The name of the month is December");
//            // do something different
//        });
//
//// Evaluate a condition and execute the corresponding action
//        System.out.println("What language do you speak");
//        System.out.println("Please enter the number of the month");
//        number = scanner.nextInt();
//
//        if (actions.containsKey(number)) {
//            actions.get(number).run();
//        }

        // Create a ResourceBundle for the user's locale
        Locale currentLocale = new Locale("fr", "FR");
        ResourceBundle months = ResourceBundle.getBundle("MonthsBundle", currentLocale);

// Retrieve the translated month names
        String january = months.getString("month.1");
        String february = months.getString("month.2");
        String march = months.getString("month.3");
// ...

// Display the translated month names
        System.out.println(january);
        System.out.println(february);
        System.out.println(march);
// ...

    }
}
