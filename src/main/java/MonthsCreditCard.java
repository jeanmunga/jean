import java.util.Scanner;
import java.util.prefs.AbstractPreferences;

public class MonthsCreditCard {
    double NumMonth;





    public static void main(String[] args) {

        double Balance;
        double APR;

        Scanner scanner = new Scanner(System.in);

        System.out.println("what is the balance");
        Balance = scanner.nextDouble();

        System.out.println("what is the APR on the card (as a percent) ? ");
        APR = scanner.nextDouble();

        System.out.print("Enter the monthly payment you plan to make: ");
        double payment = scanner.nextDouble();

        MonthsCreditCard months = new MonthsCreditCard();
        months.Calculate(Balance,APR,payment);

    }

    public int Calculate(double Balance, double APR, double MonthlyPayment){

//         double DecimalAPR = APR /100/365;
//
//        NumMonth = -(1.0/30.0) * (Math.log10(1.0 + (Math.pow(Balance, 1.0/30.0) / (1.0 - (1.0 + DecimalAPR)))) / Math.log10(1.0 + DecimalAPR));
//        System.out.println(NumMonth);
        double dailyRate = APR/365.0;

        double numerator = Math.log10(1.0 + (Balance / MonthlyPayment) * (1.0 - Math.pow(1.0 + dailyRate, -30)));
        double denominator = Math.log10(1.0 + dailyRate);

        int months = (int) Math.ceil(-1.0 / 30.0 * numerator / denominator);

        System.out.println(months);
        return months;

    }
}
