import java.util.Random;
import java.util.Scanner;

public class Guess {
    Random random = new Random();
    int maxNumber = 0;
    int randomNumber = 0;
    Scanner scanner = new Scanner(System.in);
    int numberOfGuesses = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        Random random = new Random();
//        int maxNumber = 0;
//        int numberOfGuesses = 0;
//        int randomNumber = 0;
        boolean playAgain = true;
        Guess diff = new Guess();

        while (playAgain) {
            // Prompt for difficulty level
            System.out.println("Welcome to Guess the Number game!");
            System.out.println("Choose a difficulty level:");
            System.out.println("1. Easy (1-10)");
            System.out.println("2. Medium (1-100)");
            System.out.println("3. Hard (1-1000)");
            int difficultyLevel = scanner.nextInt();

            diff.DifficultyLevel(difficultyLevel);
            diff.GuessNumber();
            
            // Ask if player wants to play again
            System.out.print("Do you want to play again? (yes/no): ");
            String playAgainInput = scanner.next().toLowerCase();
            playAgain = playAgainInput.equals("yes") || playAgainInput.equals("y");
        }

        // Game over
        System.out.println("Thanks for playing Guess the Number!");

    }

    public int DifficultyLevel(int difficultyLevel){
        switch (difficultyLevel) {
            case 1:
                maxNumber = 10;
                break;
            case 2:
                maxNumber = 100;
                break;
            case 3:
                maxNumber = 1000;
                break;
            default:
                System.out.println("Invalid difficulty level!");
//                continue;
        }

        // Pick a random number within the maximum range
        randomNumber = random.nextInt(maxNumber) + 1;
        return randomNumber;

    }

    public void GuessNumber(){

        // Reset number of guesses
        numberOfGuesses = 0;

        // Start the game
        System.out.println("I'm thinking of a number between 1 and " + maxNumber + ".");
        int guess = 0;
        while (guess != randomNumber) {
            System.out.print("Enter your guess: ");
            if (scanner.hasNextInt()) {
                guess = scanner.nextInt();
                numberOfGuesses++;

                if (guess < randomNumber) {
                    System.out.println("Too low! Try again.");
                } else if (guess > randomNumber) {
                    System.out.println("Too high! Try again.");
                } else {
                    System.out.println("Congratulations! You guessed the number in " + numberOfGuesses + " guesses!");
                    if (numberOfGuesses == 1) {
                        System.out.println("You're a mind reader!");
                    } else if (numberOfGuesses <= 4) {
                        System.out.println("Most impressive.");
                    } else if (numberOfGuesses <= 6) {
                        System.out.println("You can do better than that.");
                    } else {
                        System.out.println("Better luck next time.");
                    }
                }
            } else {
                // Count non-numeric entries as wrong guesses
                scanner.next();
                numberOfGuesses++;
                System.out.println("Invalid input! Try again.");
            }
        }

    }



}
