import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RectangleAreaGUI extends Frame {

    TextField tf1;
    TextField tf2;
    Label l1;
    Button b;
    Button c;
//    Button d;
//    Button e;
    int a;
    int bc;

    RectangleAreaGUI() {
        setTitle("Adder");
        tf1 = new TextField();
        tf1.setBounds(100, 50, 85, 20);

        tf2 = new TextField();
        tf2.setBounds(100, 110, 85, 20);

        b = new Button("Feet");
        b.setBounds(110, 200, 50, 50);

        c = new Button("Meters");
        c.setBounds(110, 260, 50, 40);

        l1 = new Label("");
        l1.setBounds(100, 120, 85, 20);


        add(b);
        add(c);

        add(tf1);
        add(tf2);
        add(l1);

        setSize(300, 300);
        setVisible(true);


        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());


                if (a < 0 || bc < 0) {
                    l1.setText("Input positive number");
                } else {

                    int c = a*bc;

                    l1.setText("Their sum is = " + String.valueOf(c));

                }

            }
        });

        c.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                a = Integer.parseInt(tf1.getText());
                bc = Integer.parseInt(tf2.getText());


                if (a < 0 || bc < 0) {
                    l1.setText("Input positive number");
                } else {

                    double c = a*bc * 0.09290304;

                    l1.setText("Their sum is = " + Math.round(c * 1000.0) / 1000.0);

                }

            }
        });
    }






    public static void main(String[] args) {
//           int area, width, height;
//           String input = JOptionPane.showInputDialog(null,
//                   "Enter width ", "Rectangle Area Input",
//                   JOptionPane.PLAIN_MESSAGE);
//          width = Integer.parseInt(input);
//
//          input = JOptionPane.showInputDialog(null,
//                      "Enter height ", "Rectangle Area Input",
//                      JOptionPane.PLAIN_MESSAGE);
//          height = Integer.parseInt(input);
//          area = width * height;
//         JOptionPane.showMessageDialog(null,
//                      "Area of Rectangle: " + area,
//                     "Rectangle Area Output",
//                        JOptionPane.PLAIN_MESSAGE);
//         }

        new RectangleAreaGUI();
    }



}
