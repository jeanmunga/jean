import jdk.swing.interop.SwingInterOpUtils;

import java.util.Objects;
import java.util.Scanner;

public class TemperatureConverter {

    double Fahrenheit;
    double Celsius;
    double Kelvin;
    double startTemp;
    String input;

    Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        double startTemp;
        String input;

        Scanner scanner = new Scanner(System.in);

        TemperatureConverter temperatureConverter = new TemperatureConverter();

        System.out.println("Convert your temperature from Fahrenheit to Celsius or from Celsius to Fahrenheit.");
        input = scanner.next();

        if(input.equalsIgnoreCase("C")){
            System.out.println("What is your temperature in Celsius");
            startTemp = scanner.nextDouble();
            temperatureConverter.Celsius(startTemp);

        }else if(input.equalsIgnoreCase("F")){
            System.out.println("What is your temperature in Fahrenheit");
            startTemp = scanner.nextDouble();
            temperatureConverter.Fahrenheit(startTemp);

        }
        else if(input.equalsIgnoreCase("CK")){
            System.out.println("What is your temperature in Celsius");
            startTemp = scanner.nextDouble();
            temperatureConverter.CelsiusToKelvin(startTemp);

        }
        else if(input.equalsIgnoreCase("CF")){
            System.out.println("What is your temperature in Fahrenheit");
            startTemp = scanner.nextDouble();
            temperatureConverter.FahrenheitToKelvin(startTemp);
        }
        
    }

    public void Celsius(double startTemp) {

        Celsius  =(startTemp -32) * 5 / 9;
        System.out.println(Celsius);
    }

    public void Fahrenheit(double startTemp){

        Fahrenheit = (startTemp * 9 / 5) + 32;
        System.out.println(Fahrenheit);
    }

    public void CelsiusToKelvin(double startTemp) {

        Kelvin  =startTemp + 273.15;
        System.out.println(Kelvin);
    }

    public void FahrenheitToKelvin(double startTemp){

        Kelvin  =(startTemp -32) * 5 / 9 + 273.15;
        System.out.println(Kelvin);
    }
}
