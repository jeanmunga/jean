import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CurrencyConversion {
    public static void main(String[] args) {
        double euros;
        double exchangeRate;
        String country;

        Scanner scanner = new Scanner(System.in);

        System.out.println("How many euros are you exchanging? ");
        euros = scanner.nextDouble();

        System.out.println("what is the country");
        country = scanner.nextLine();


        Map<String, Double> map = new HashMap<String, Double>();
        map.put("USD", 137.81);

        exchangeRate = map.get("USD");
        if (map.containsKey(country)) {

            double calculation = euros * exchangeRate / 100;
            System.out.println(euros + " euros at an exchange rate of " + exchangeRate + " is " + calculation + " U.S. dollars.");
        } else {
            System.out.println("input invalid");
        }

    }

}
