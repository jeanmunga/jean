import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class test {
    @Test
    void AdditionTest() {
        LearnAWT world= new LearnAWT();
        assertEquals(4, world.add(2,2));

    }

    @Test
    void MinusTest() {
        LearnAWT world= new LearnAWT();
        assertEquals(8, world.minus(10,2));

    }

    @Test
    void MultiplicationTest() {
        LearnAWT world= new LearnAWT();
        assertEquals(20, world.multiple(10,2));

    }

    @Test
    void DivisionTest() {
        LearnAWT world= new LearnAWT();
        assertEquals(5, world.divide(10,2));

    }

}
